package com.tt.controller;

import com.sun.javafx.collections.MappingChange;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: blackcat
 * @Date: 2019/11/15
 * @Description: com.tt.controller
 * @version:
 */
@RestController
public class DemoController {
    @RequestMapping("demo")
    public Map<String,String>mapJson(){
        Map<String,String> map = new HashMap<>();
        map.put("1","100");
        map.put("2","200");
        map.put("3","300");
        map.put("4","400");
        return map;
    }

}
